# gitlab-ci-provision

This provisions a machine on AWS and starts a gitlab runner on it.


## Dependencies

```bash
# install ansible https://github.com/ansible/ansible
pip install ansible
```

## Provisioning

This requires two machines, a `master` and a `slave`. I have it set up now
so that `master` is running ubuntu and `slave` is running coreos.

```bash
# Add the IP addresses/host names for your server 
echo '
[master]
some-server.www/ip address
' > hosts

# Generate and save a runner token
echo "
---
gitlab_ci_runner_token_docker: $(docker run --rm -e TOKEN=dAv-rdhTuv_rfs9xm2e4 -e TAGS=docker quay.io/saulshanabrook/gitlab-ci-token)
gitlab_ci_runner_token_dind_isolated: $(docker run --rm -e TOKEN=dAv-rdhTuv_rfs9xm2e4 -e TAGS=dind_isolated quay.io/saulshanabrook/gitlab-ci-token)
" > group_vars/master

ansible-playbook playbook.yml
```

To update the docker image, change the `docker_image` variable in `./group_vars/all`
and run `ansible-playbook playbook.yml -l master`

## Debugging

Gitlab CI logs:

```bash
ansible master -m command -a 'cat /var/log/syslog | grep gitlab-ci-runner | tail -n 50'
```

Gitlab CI config:

```bash
ansible master -m command -a 'cat /etc/gitlab-ci/config.toml'
```

Specific Run logs

```bash
# find the name or id of the run container
ansible master -m command -a 'docker ps'

# then get it's logs
ansible master -m command -a 'docker logs runner-27bbf33a-project-532380-concurrent-0-build'
```

Force triger cleanup (run every hour on Cron)

```bash
ansible master -m command -a 'docker-cleanup.sh'
```

## Troubleshooting

If you get `ERROR: Build failed with: API error (500): Could not find container for entity id ece5da9419c9c23b5d0cdacf3e065066bd1b672c861a305b1c1eeb4844fcb024`
on a build run ([will be fixed in Docker 1.10](https://github.com/docker/docker/issues/17691)) 

```bash
ansible master -m command -a 'sudo rm -f /var/lib/docker/linkgraph.db'
ansible master -m command -a 'sudo rm -rf /var/lib/docker/containers/'
ansible master -m command -a 'sudo restart docker'
```


If you get something like this:

```
Handler for POST /v1.21/networks/create returned error: failed to parse pool request for address space \"LocalDefault\" pool \"\" subpool \"
HTTP Error" err="failed to parse pool request for address space \"LocalDefault\" pool \"\" subpool \"\": could not find an available predefi
```

[the problem is that too many networks were left around](https://github.com/docker/compose/issues/2279#issuecomment-159220844).

You can delete all the docker compose network with:

```bash
ansible master -m shell -a 'docker network ls | grep dind | sed 's/ .*//g' | xargs -n 1 docker network rm'
```
