package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/satori/go.uuid"
)

func openingMessage() {
	fmt.Println("--> Running build in quay.io/thelabnyc/docker-compose:latest image")
	fmt.Println()

	fmt.Printf("--> 'docker version'\n")
	cmd := exec.Command("docker", "version")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
	fmt.Println()

	fmt.Printf("--> 'docker-compose version'\n")
	cmd = exec.Command("docker-compose", "version")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
	fmt.Println()

}

func generateComposeName() string {
	name := fmt.Sprintf(
		"dind%v",
		strings.Replace(uuid.NewV4().String(), "-", "", -1),
	)
	fmt.Printf("--> Using '%v' as Docker Compose project name\n\n", name)
	return name
}

func processCmdErr(err error) (exitCode int) {
	if err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok {
			fmt.Printf("--> Failed because: '%v'\n", exitErr.Error())
			return exitErr.Sys().(syscall.WaitStatus).ExitStatus()
		}
		panic(fmt.Sprintf(
			"--> Failed to execute the command. This means the runner had a problem "+
				"starting up the command. The runner is using the "+
				"quay.io/thelabnyc/docker-compose:latestr image. It raised the error:\n"+
				"%v\n",
			err.Error(),
		))
	}
	fmt.Println("--> Succeeded")
	return 0
}

func runScript(composeName string) (exitCode int) {
	cmd := exec.Command(os.Args[1], os.Args[2:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = append(os.Environ(), fmt.Sprintf("COMPOSE_PROJECT_NAME=%v", composeName))

	return processCmdErr(cmd.Run())
}

func cleanUp(composeName string) {
	script := fmt.Sprintf(
		"docker rm -fv $(docker ps -a --format '{{.Names}}' | grep %v)",
		composeName,
	)
	fmt.Printf("--> Cleaning up: '%v'\n", script)
	cmd := exec.Command("bash", "-c", script)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	processCmdErr(cmd.Run())
}

func main() {
	// Gitlab CI passes in "bash" as the first argument and the source
	// of a bash script as stdin. We will execute this command the same as it
	// does and then just do some cleanup
	openingMessage()
	composeName := generateComposeName()
	exitCode := runScript(composeName)
	cleanUp(composeName)
	os.Exit(exitCode)
}
