#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o xtrace

echo 'First output'

sleep 5m

echo 'Second output'
