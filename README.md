# Docker

- Create Amazon Linux EC2 - t series is burstable CPU and good for build servers.
- Install docker and set to overlayfs
- Create 2 EBS volumes. A small one for the OS and a big one just for docker.
http://blog.cloud66.com/docker-with-overlayfs-first-impression/
- partition it as the blog post describes
example: mkfs -t ext4 -N 16568256 /dev/xvdf
- Mount new volume
mount /dev/xvdf /var/lib/docker/overlay
- Forever on fstab
/dev/xvdf   /var/lib/docker/overlay   ext4    defaults,nofail        0       2  

# Cleanup

https://gitlab.com/gitlab-org/gitlab-runner-docker-cleanup

# Executors

We enable both docker and shell executor. 
Docker is easier to use and more reliable but can't use docker-compose. It's fine to use either.

# Gitlab CI 

Install it https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/linux-repository.md
Run as root to avoid common issues. Just use a dedicated VM so it doesn't matter.

# Old info below - do not use

```





```



# Continuous Integration

If you want to run tests and possibly deploy your code automatically, we have
support for [GitLab Continuous Integration](https://about.gitlab.com/gitlab-ci/).

You should follow [their Quick Start guide](http://doc.gitlab.com/ce/ci/quick_start/README.html).

## Build Environment

You have access to a runner, which is deployed using the Ansible scripts
in [`./ansible`](./ansible). Make sure to disable shared runners
so that it will only use this docker in `docker-compose` image.

It will run your container in a an clean environment, with Docker (1.10.0),
Docker Compose (1.6.0), and [Dpl](https://github.com/travis-ci/dpl)
[installed](./dockerfiles/docker-compose/Dockerfile).

You don't have to clean up your Docker Compose container's before or after your
build, since
[it will for you](./dockerfiles/docker-compose/docker-entrypoint.sh). It stops
and removes all containers that have `$COMPOSE_PROJECT_NAME` in their name,
which is all containers started by Docker Compose, unless you override their
name. If you do, you should include `$COMPOSE_PROJECT_NAME` in the new name
so they get cleaned up properly.


### Types
There are two different ways to run your builds if you want to run `docker`
commands.

The most fault resistant is `dind_isolated`. This runs each build with it's
own isolated Docker daemon, so that you can be sure that there is no leftover
state. It is also easier to clean up, because we just trash the whole Docker
environment.

If the build times are too long, however, you can use the `docker` runner instead.
It runs all builds against the same docker daemon and tries to do a bit of
cleanup afterword, like deleting all Docker Compose containers. Since it
shares a Docker daemon between builds, `docker pull` and `docker build` will
both be cached.

Use the `tags` keyword in your `.gitlab.ci.yml` to select which runner you want
to use.

```yaml
job1:
  stage: build
  script:
    - execute-script-for-job1
  tags
    - dind_isolated
``` 

### Modifying

In order to install something else on the base build image, you should:

1. Modify build image in [`./dockerfiles/docker-compose`](./dockerfiles/docker-compose)
2. Add new git commit and push
3. Wait for [Quay to finish building this image](https://quay.io/repository/thelabnyc/docker-compose?tab=builds)


## Best Practices

### Databases

If you try to access a database too quickly after it is started, in your tests
they can fail. This can happen if you try something like  `docker-compose run web python manage.py test`.

#### Waiting for port

There are a couple of ways to avoid this. The most full proof method is to
make sure your Python command doesn't run until the database port is open.

You can do this by creating a overriding the entrypoint on your container
to wait for that port to open. Add this file to your repository and make
it executable (`chmod +x`).

```bash
#!/usr/bin/env bash

# wait for db to come up before starting tests, as shown in https://github.com/docker/compose/issues/374#issuecomment-126312313
# uses bash instead of netcat, because netcat is less likely to be installed
# strategy from http://superuser.com/a/806331/98716
set -e

echoerr() { echo "$@" 1>&2; }

timeout 15 bash <<EOT
while ! (echo > /dev/tcp/db/5432) >/dev/null 2>&1;
    do sleep 1;
done;
EOT
RESULT=$?

if [ $RESULT -eq 0 ]; then
  # sleep another second for so that we don't get a "the database system is start up" error
  sleep 1
  echoerr wait-for-db: done
else
  echoerr wait-for-db: timeout out after 10 seconds waiting for db:5432
fi


exec "$@"
```

Then change the `entrypoint` in your `docker-compose.yml` to use this file.

See [`./examples/docker-compose-links`](./examples/docker-compose-links) for
a working example of this.

### Ports

Don't enable any host port mapping for containers that are run in CI. Because
the tests might run on the same Docker daemon, doing so might cause conflicts
between simultaniously running tests.

For example, instead of mapping ports for all containers:

```bash
$ cat docker-compose.yml
web:
  build .
  ports:
    - 8000:8000
$ cat .gitlab.ci.yml
test:
  script:
    - docker run web python manage.py test
```

Just do it in development:

```bash
$ cat docker-compose.yml
web:
  build .
$ cat docker-compose.override.yml
web:
  ports:
    - 8000:8000
$ cat .gitlab.ci.yml
test:
  script:
    - rm docker-compose.override.yml
    - docker run web python manage.py test
```

### Node

If you are running `npm install` please use `npm --loglevel warn install`
instead, or else the test logs can become flooded and unwieldy. 


### Debugging

If the tests seem to fail unexpectedly, and exit with codee 137, they are
likely being killed because of resource constraints. It might be that the
ram has been exhausted on the test running machine. If this is the case,
you can increase the swap (created in `./ansible/roles/coreos.swap`) or
upgrade the machine.  

## Upgrading Docker
We need to upgrade the runner images as well as the docker running on the CI
machine. If only one is upgraded, Docker might refuse to run in the tests,
because of a version mismatch between the server and client.

### On the CI machine

First `cd ansible`

1. (optional) Stop and wipe docker: `ansible master -m shell -a 'sudo stop docker; rm -rf /var/lib/docker'`
`cd ansible; 
2. Upgrade the Docker package: `ansible master -m command -a 'sudo apt-get install -y --only-upgrade docker'`
3. Start up Docker: `ansible master -m command -a 'sudo start docker'`

### The runner images
1. Update the base images in
   [`./dockerfiles/dind-isolated/Dockerfile`](./dockerfiles/dind-isolated/Dockerfile)
   and
   [`./dockerfiles/docker-compose/Dockerfile`](./dockerfiles/docker-compose/Dockerfile)
   to the [new Docker image](https://hub.docker.com/_/docker/).
2. Rebuild those and push to their Quay.io paths. You can also just commit and
   push to Github, and Quay.io *should* build them automatically.
